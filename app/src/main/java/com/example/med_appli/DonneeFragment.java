package com.example.med_appli;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.med_appli.adapter.basedonnee.GestionBD;
import com.example.med_appli.beans.Donnee;
import com.example.med_appli.controller.RequeteDonnee;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static com.example.med_appli.recordFragment.getDebutetFin;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DonneeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DonneeFragment extends Fragment implements OnChartValueSelectedListener, OnChartGestureListener {

    LineChart lineChart;
    TableLayout table;

    EditText dateDebut, dateFin;
    Button btn_rech;
    private int day,month,year;
    private Calendar mcalendar;
    private  Button btn_impr;
    donneeRefresh mescontact;

    LinearLayout layout_graph;

    String date1;
    String date2;

    List<Entry> listDonnee;
    ArrayList<Donnee> listDonne2;

    boolean ref = true;

    View view;

    public static final String RESULT = "./festival_opening.pdf";

    RequeteDonnee req;

    GestionBD access;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public DonneeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DonneeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DonneeFragment newInstance(String param1, String param2) {
        DonneeFragment fragment = new DonneeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_donnee, container, false);

        layout_graph = view.findViewById(R.id.layout_graph);

        btn_impr = view.findViewById(R.id.btn_impr);

        btn_impr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M){
                   if (ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED ||
                   ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED){

                       String[] permission = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
                               Manifest.permission.READ_EXTERNAL_STORAGE};
                       ActivityCompat.requestPermissions(getActivity(), permission, 1000);

                   } else {
                           genererPDF();
                   }
               } else {
                       genererPDF();
               }
            }
        });

        req = new RequeteDonnee();
        access = new GestionBD(view.getContext());

        mcalendar = Calendar.getInstance();

        lineChart = view.findViewById(R.id.id_chart);
        lineChart.setTouchEnabled(true);
        lineChart.setPinchZoom(true);
        lineChart.setOnChartGestureListener(DonneeFragment.this);
        lineChart.setOnChartValueSelectedListener(DonneeFragment.this);
        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(false);

        dateDebut = view.findViewById(R.id.date1);
        dateFin = view.findViewById(R.id.date2);
        btn_rech = view.findViewById(R.id.btn_rech);

        dateDebut.setOnClickListener(mClickListener);
        dateFin.setOnClickListener(mClickListener1);
        btn_rech.setOnClickListener(btnClickListener);

        day=mcalendar.get(Calendar.DAY_OF_MONTH);
        year=mcalendar.get(Calendar.YEAR);
        month=mcalendar.get(Calendar.MONTH);

        int day1 = mcalendar.getTime().getDay();

        table = view.findViewById(R.id.tablayout1);
        final Format f = new SimpleDateFormat("yyyy/MM/dd");

        String lesDates = getDebutetFin(day1);
        date1 = lesDates.split("-")[0];
        date2 = lesDates.split("-")[1];

        listDonnee = new RequeteDonnee().getListDonnee1(date1, date2, new GestionBD(view.getContext()));
        listDonne2 = new RequeteDonnee().getListDonnee(date1, date2, new GestionBD(view.getContext()));
        System.out.println(Integer.parseInt(date1.split("/")[2])+" oooo  "+Integer.parseInt(date2.split("/")[2]));
        //renderData(Integer.parseInt(date1.split("/")[2]), Integer.parseInt(date2.split("/")[2]));
        remplirTableau(table, listDonne2, view);
        remplirTab(listDonnee);

        mescontact = new donneeRefresh();
        mescontact.run();

        return view;

    }

    View.OnClickListener mClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DateDialog();
        }
    };

    View.OnClickListener mClickListener1=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DateDialog1();
        }
    };

    View.OnClickListener btnClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
             date1 = dateDebut.getText().toString();
             date2 = dateFin.getText().toString();

            listDonne2 = req.getListDonnee(date1, date2, access);
            listDonnee = req.getListDonnee1(date1, date2, access);
            table.removeViews(1, Math.max(0, table.getChildCount() - 1));

            remplirTab(listDonnee);
            remplirTableau(table, listDonne2, view);
            ref = false;
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 2: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                        genererPDF();
                } else {
                    Toast.makeText(view.getContext(), "Permission refusée vous ne pouvez generer de pdf", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    public void DateDialog(){
        DatePickerDialog.OnDateSetListener listener=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
            {
                dateDebut.setText(year + "/" + (monthOfYear+1) + "/" + dayOfMonth);
            }};
        DatePickerDialog dpDialog=new DatePickerDialog(getActivity(), listener, year, month, day);
        dpDialog.show();
    }

    public void DateDialog1(){
        DatePickerDialog.OnDateSetListener listener=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
            {
                dateFin.setText(year + "/" + (monthOfYear+1) + "/" + dayOfMonth);
            }};
        DatePickerDialog dpDialog=new DatePickerDialog(getActivity(), listener, year, month, day);
        dpDialog.show();
    }

    public void remplirTab( List<Entry> value){
        LineDataSet set1 = new LineDataSet(value, "Data set 1");
        set1.setFillAlpha(110);
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);
        LineData data = new LineData(dataSets);
        lineChart.setData(data);
    }

     public void remplirTableau(TableLayout table, ArrayList<Donnee> listDonnee, View view){

        for (Donnee donnee : listDonnee) {
            TableRow tr = new TableRow(view.getContext());
            tr.setGravity(Gravity.CENTER);

            TextView t1 = new TextView(view.getContext());
            t1.setText(donnee.getDate());
            t1.setGravity(Gravity.CENTER);
            tr.addView(t1);

            TextView t2 = new TextView(view.getContext());
            t2.setText(donnee.getSysto()+"");
            t2.setGravity(Gravity.CENTER);
            tr.addView(t2);

            TextView t3 = new TextView(view.getContext());
            t3.setText(donnee.getDiasto()+"");
            t3.setGravity(Gravity.CENTER);
            tr.addView(t3);

            TextView t4 = new TextView(view.getContext());
            t4.setText(donnee.getPoul()+"");
            t4.setGravity(Gravity.CENTER);
            tr.addView(t4);

            TextView t5 = new TextView(view.getContext());
            t5.setText(donnee.getPoids()+"");
            t5.setGravity(Gravity.CENTER);
            tr.addView(t5);

            TextView t6 = new TextView(view.getContext());
            t6.setText(donnee.getComment()+"");
            t6.setGravity(Gravity.CENTER);
            tr.addView(t6);

            table.addView(tr);
        }
    }

    public void genererPDF() {
        Document document = new Document();

        String fileName = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault())
                .format(System.currentTimeMillis());

        String mFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + fileName + ".pdf";

        try {
            PdfWriter.getInstance(document, new FileOutputStream(mFilePath));
            document.open();

            Paragraph titre = new Paragraph("Bilan de suivi de la semaine",
                    new Font(Font.FontFamily.TIMES_ROMAN, 20, 3));

            Paragraph periode = new Paragraph(date1+" au "+date2,
                    new Font(Font.FontFamily.TIMES_ROMAN, 20, 3));

            Paragraph tabRecap = new Paragraph("Tableau recapitulatif des enregistrement",
                    new Font(Font.FontFamily.TIMES_ROMAN, 16, 3));

            Paragraph graphRecap = new Paragraph("Graphe montrant l'evolution de la diastolique au cour de cette periode",
                    new Font(Font.FontFamily.TIMES_ROMAN, 16, 3));
            titre.setAlignment(Element.ALIGN_CENTER);
            titre.setSpacingAfter(10);

            periode.setAlignment(Element.ALIGN_CENTER);
            periode.setSpacingAfter(10);

            tabRecap.setAlignment(Element.ALIGN_CENTER);
            tabRecap.setSpacingAfter(10);

            graphRecap.setAlignment(Element.ALIGN_CENTER);

            PdfPTable table = new PdfPTable(6);

            //creation des entetes
            PdfPCell c1 = new PdfPCell(new Paragraph("Date du jour"));
            PdfPCell c2 = new PdfPCell(new Paragraph("Systolique"));
            PdfPCell c3 = new PdfPCell(new Paragraph("Diastolique"));
            PdfPCell c4 = new PdfPCell(new Paragraph("Pouls"));
            PdfPCell c5 = new PdfPCell(new Paragraph("Poids"));
            PdfPCell c6 = new PdfPCell(new Paragraph("Commentaire"));

            table.addCell(c1);
            table.addCell(c2);
            table.addCell(c3);
            table.addCell(c4);
            table.addCell(c5);
            table.addCell(c6);

            for (Donnee d : listDonne2) {
                PdfPCell c11 = new PdfPCell(new Paragraph(d.getDate()));
                PdfPCell c22 = new PdfPCell(new Paragraph(d.getSysto()+""));
                PdfPCell c33 = new PdfPCell(new Paragraph(d.getDiasto()+""));
                PdfPCell c44 = new PdfPCell(new Paragraph(d.getPoul()+""));
                PdfPCell c55 = new PdfPCell(new Paragraph(d.getPoids()+""));
                PdfPCell c66 = new PdfPCell(new Paragraph(d.getComment()));

                table.addCell(c11);
                table.addCell(c22);
                table.addCell(c33);
                table.addCell(c44);
                table.addCell(c55);
                table.addCell(c66);
            }


            String path = Environment.getExternalStorageDirectory().toString();
            OutputStream fOutputStream = null;
            File file = new File(path, fileName+"f.pdf");
            if (file.exists()) {
                file.mkdirs();
            }

            Bitmap bitmap = convertGraphImage(lineChart);

            fOutputStream = new FileOutputStream(file);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOutputStream);

            fOutputStream.flush();
            fOutputStream.close();

            MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());

            String test = file.getAbsolutePath();
            Image image = Image.getInstance(file.getAbsolutePath());

            document.addAuthor("create by leDecolo");
            document.add(titre);
            document.add(periode);
            document.add(tabRecap);
            document.add(new Paragraph(" "));
            document.add(table);
            document.add(new Paragraph(" "));
            document.add(graphRecap);
            document.add(new Paragraph(" "));
            document.add(image);

            document.close();

            Toast.makeText(view.getContext(), fileName+".pdf\n est cree dans le dossier "+mFilePath, Toast.LENGTH_LONG).show();

            Intent intent = new Intent(view.getContext(), OpenPDF.class);
            intent.putExtra("chemin", fileName+".pdf");
            startActivity(intent);
        } catch (Exception e){
            Toast.makeText(view.getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            System.out.println("------"+e.getMessage());
        }
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartLongPressed(MotionEvent me) {

    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {

    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {

    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {

    }

    public class donneeRefresh extends Thread{
        @Override
        public void run() {
            super.run();

                int day1 = mcalendar.getTime().getDay();
                String lesDates = getDebutetFin(day1);
                String date1 = lesDates.split("-")[0];
                String date2 = lesDates.split("-")[1];
                List<Entry> listDonnee = new RequeteDonnee().getListDonnee1(date1, date2, new GestionBD(view.getContext()));
                remplirTab(listDonnee);

        }
    }

    public Bitmap convertGraphImage(View view1) {

        Bitmap bitmap = Bitmap.createBitmap(view1.getWidth(), view1.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Drawable bgDrawable = view1.getBackground();
         if (bgDrawable != null){
             bgDrawable.draw(canvas);
         }else{
             canvas.drawColor(Color.WHITE);
         }
        view1.draw(canvas);
        return bitmap;
    }
}