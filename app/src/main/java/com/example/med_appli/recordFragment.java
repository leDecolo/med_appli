package com.example.med_appli;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.med_appli.adapter.basedonnee.GestionBD;
import com.example.med_appli.beans.Donnee;
import com.example.med_appli.controller.RequeteDonnee;

import java.text.DateFormatSymbols;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link recordFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class recordFragment extends Fragment {
    TextView val_sys;
    TextView val_dias;
    TextView val_poul;
    TextView val_pd;
    TextView val_com;
    Button btn_val;
    RequeteDonnee req;
    GestionBD gestion;
    TableLayout table;
    View view;
    int reponse = -1;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public recordFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment recordFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static recordFragment newInstance(String param1, String param2) {
        recordFragment fragment = new recordFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

       view = inflater.inflate(R.layout.fragment_record, container, false);

        val_sys = view.findViewById(R.id.id_systo);
        val_dias = view.findViewById(R.id.id_diasto);
        val_poul = view.findViewById(R.id.id_pouls);
        val_pd = view.findViewById(R.id.id_poids);
        val_com = view.findViewById(R.id.id_commentaire);
        btn_val = view.findViewById(R.id.btn_val);

        req = new RequeteDonnee();
        gestion = new GestionBD(getActivity().getApplicationContext());


        table = view.findViewById(R.id.tablayout);


        final Format f = new SimpleDateFormat("yyyy/MM/dd");

        Calendar calendar = Calendar.getInstance();
        final Date date = calendar.getTime();
        int day = date.getDay();
        String lesDates = getDebutetFin(day);

        final String date1 = lesDates.split("-")[0];
        final String date2 = lesDates.split("-")[1];

        ArrayList<Donnee> listdonnee = req.getListDonnee(date1, date2, gestion);

        remplirTableau(f.format(date), table, listdonnee, view);


        btn_val.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if(val_sys.getText().toString().trim().equals("") || val_dias.getText().toString().trim().equals("") || val_poul.getText().toString().trim().equals("")){
                    Toast.makeText(getActivity().getApplicationContext(), "Veuillez remplir tous les champs obligatoires", Toast.LENGTH_LONG).show();
                } else {

                    int verif = req.verifPoids(date1, date2, gestion);
                    boolean verifDate = req.verifDate(f.format(date), gestion);

                    //revoir la situation de l'instruction ci

                    if(verifDate){
                        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                        builder.setTitle("Demande de confirmation");
                        builder.setMessage("vous avez deja fait un enregistrement a cette date voulez-vous modifier?");
                        builder.setIcon(android.R.drawable.ic_dialog_alert);
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Donnee donnee = new Donnee(f.format(date),
                                        Float.parseFloat(val_sys.getText().toString()),
                                        Float.parseFloat(val_dias.getText().toString()),
                                        Float.parseFloat(val_poul.getText().toString()),
                                        (val_pd.getText().toString().trim().equals(""))?0:Float.parseFloat(val_pd.getText().toString()),
                                        (val_com.getText().toString().trim().equals(""))? "" : val_com.getText().toString());

                                req.modifDonnee(donnee, gestion);
                                val_pd.setText("");
                                val_poul.setText("");
                                val_sys.setText("");
                                val_dias.setText("");
                                val_com.setText("");

                                Toast.makeText(getActivity().getApplicationContext(), "Enregistrement termine avec success", Toast.LENGTH_SHORT).show();
                            }
                        });
                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                System.out.println("click sur no");
                            }
                        });


                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();

                    }

                    else if (verif == 0 && val_pd.getText().toString().trim().equals("")){
                            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                            builder.setTitle("Demande de confirmation");
                            builder.setMessage("Vous n'avez pas encore pris votre poids hebdo voulez-vous continuer?");
                            builder.setIcon(android.R.drawable.ic_dialog_alert);
                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Donnee donnee = new Donnee(f.format(date),
                                            Float.parseFloat(val_sys.getText().toString()),
                                            Float.parseFloat(val_dias.getText().toString()),
                                            Float.parseFloat(val_poul.getText().toString()),
                                            0,
                                            (val_com.getText().toString().trim().equals(""))? "" : val_com.getText().toString());

                                    req.ajout_donnee(donnee, gestion);
                                    updateTableau(table, donnee, view);
                                    val_pd.setText("");
                                    val_poul.setText("");
                                    val_sys.setText("");
                                    val_dias.setText("");
                                    val_com.setText("");

                                    Toast.makeText(getActivity().getApplicationContext(), "Enregistrement termine avec success", Toast.LENGTH_SHORT).show();
                                }
                            });
                            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    System.out.println("click sur no");
                                }
                            });

                            AlertDialog alertDialog = builder.create();
                            alertDialog.show();
                    }else {

                        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                        builder.setTitle("Demande de confirmation");
                        builder.setMessage("êtes-vous sur de données enregistrées");
                        builder.setIcon(android.R.drawable.ic_dialog_alert);
                        builder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Donnee donnee = new Donnee(f.format(date),
                                        Float.parseFloat(val_sys.getText().toString()),
                                        Float.parseFloat(val_dias.getText().toString()),
                                        Float.parseFloat(val_poul.getText().toString()),
                                        (val_pd.getText().toString().trim().equals(""))?0:Float.parseFloat(val_pd.getText().toString()),
                                        (val_com.getText().toString().trim().equals(""))? "" : val_com.getText().toString());

                                req.ajout_donnee(donnee, gestion);
                                updateTableau(table, donnee, view);
                                val_pd.setText("");
                                val_poul.setText("");
                                val_sys.setText("");
                                val_dias.setText("");
                                val_com.setText("");

                                Toast.makeText(getActivity().getApplicationContext(), "Enregistrement termine avec success", Toast.LENGTH_SHORT).show();
                            }
                        });
                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                System.out.println("click sur no");
                            }
                        });


                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();

                        }
                }
            }
        });
        return view;
    }

    public static String getDebutetFin(int day){
        Calendar calendar = Calendar.getInstance();
        Calendar calendar1 = Calendar.getInstance();
        Format format = new SimpleDateFormat("yyyy/MM/dd");
        String debut0;
        String fin;
        switch (day) {
            case 0:
                debut0 = format.format(calendar.getTime());
                calendar.add(Calendar.DATE, -6);
                fin = format.format(calendar.getTime());
                return fin+"-"+debut0;
            case 1:
                debut0 = format.format(calendar.getTime());
                calendar.add(Calendar.DATE, +6);
                fin = format.format(calendar.getTime());
                return debut0+"-"+fin;
            case 2:
                calendar.add(Calendar.DATE, -1);
                calendar1.add(Calendar.DATE, +5);
                debut0 = format.format(calendar.getTime());
                fin = format.format(calendar1.getTime());
                return debut0+"-"+fin;
            case 3:
                calendar.add(Calendar.DATE, -2);
                calendar1.add(Calendar.DATE, +4);
                debut0 = format.format(calendar.getTime());
                fin = format.format(calendar1.getTime());
                return debut0+"-"+fin;
            case 4:
                calendar.add(Calendar.DATE, -3);
                calendar1.add(Calendar.DATE, +3);
                debut0 = format.format(calendar.getTime());
                fin = format.format(calendar1.getTime());
                return debut0+"-"+fin;
            case 5:
                calendar.add(Calendar.DATE, -4);
                calendar1.add(Calendar.DATE, +2);
                debut0 = format.format(calendar.getTime());
                fin = format.format(calendar1.getTime());
                return debut0+"-"+fin;
            case 6:
                calendar.add(Calendar.DATE, -5);
                calendar1.add(Calendar.DATE, +1);
                debut0 = format.format(calendar.getTime());
                fin = format.format(calendar1.getTime());
                return debut0+"-"+fin;
            default:
                return "neant";
        }

    }

    public void remplirTableau(String date, TableLayout table, ArrayList<Donnee> listDonnee, View view){

        for (Donnee donnee : listDonnee) {
            TableRow tr = new TableRow(view.getContext());
            tr.setGravity(Gravity.CENTER);

            TextView t1 = new TextView(view.getContext());
            t1.setText(donnee.getDate());
            t1.setGravity(Gravity.CENTER);
            tr.addView(t1);

            TextView t2 = new TextView(view.getContext());
            t2.setText(donnee.getSysto()+"");
            t2.setGravity(Gravity.CENTER);
            tr.addView(t2);

            TextView t3 = new TextView(view.getContext());
            t3.setText(donnee.getDiasto()+"");
            t3.setGravity(Gravity.CENTER);
            tr.addView(t3);

            TextView t4 = new TextView(view.getContext());
            t4.setText(donnee.getPoul()+"");
            t4.setGravity(Gravity.CENTER);
            tr.addView(t4);

            TextView t5 = new TextView(view.getContext());
            t5.setText(donnee.getPoids()+"");
            t5.setGravity(Gravity.CENTER);
            tr.addView(t5);

            TextView t6 = new TextView(view.getContext());
            t6.setText(donnee.getComment()+"");
            t6.setGravity(Gravity.CENTER);
            tr.addView(t6);

            table.addView(tr);
        }
    }

    public void updateTableau(TableLayout table, Donnee donnee, View view){

            TableRow tr = new TableRow(view.getContext());
            tr.setGravity(Gravity.CENTER);

            TextView t1 = new TextView(view.getContext());
            t1.setText(donnee.getDate());
            t1.setGravity(Gravity.CENTER);
            tr.addView(t1);

            TextView t2 = new TextView(view.getContext());
            t2.setText(donnee.getSysto()+"");
            t2.setGravity(Gravity.CENTER);
            tr.addView(t2);

            TextView t3 = new TextView(view.getContext());
            t3.setText(donnee.getDiasto()+"");
            t3.setGravity(Gravity.CENTER);
            tr.addView(t3);

            TextView t4 = new TextView(view.getContext());
            t4.setText(donnee.getPoul()+"");
            t4.setGravity(Gravity.CENTER);
            tr.addView(t4);

            TextView t5 = new TextView(view.getContext());
            t5.setText(donnee.getPoids()+"");
            t5.setGravity(Gravity.CENTER);
            tr.addView(t5);

            TextView t6 = new TextView(view.getContext());
            t6.setText(donnee.getComment()+"");
            t6.setGravity(Gravity.CENTER);
            tr.addView(t6);

            table.addView(tr);
    }
    }