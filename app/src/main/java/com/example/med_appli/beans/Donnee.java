package com.example.med_appli.beans;

public class Donnee {
    int id;
    String date;
    float systo;
    float diasto;
    float poul;
    float poids;
    String comment;

    public Donnee(String date, float systo, float diasto, float poul, float poids, String comment) {
        this.date = date;
        this.systo = systo;
        this.diasto = diasto;
        this.poul = poul;
        this.poids = poids;
        this.comment = comment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public float getSysto() {
        return systo;
    }

    public void setSysto(float systo) {
        this.systo = systo;
    }

    public float getDiasto() {
        return diasto;
    }

    public void setDiasto(float diasto) {
        this.diasto = diasto;
    }

    public float getPoul() {
        return poul;
    }

    public void setPoul(float poul) {
        this.poul = poul;
    }

    public float getPoids() {
        return poids;
    }

    public void setPoids(float poids) {
        this.poids = poids;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
