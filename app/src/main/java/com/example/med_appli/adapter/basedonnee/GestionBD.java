package com.example.med_appli.adapter.basedonnee;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class GestionBD extends SQLiteOpenHelper {

    public static final String TABLE_COMMENTS = "donnee";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_SYST = "systolique";
    public static final String COLUMN_DIAST = "diastolique";
    public static final String COLUMN_POULS = "pouls";
    public static final String COLUMN_POIDS = "poids";
    public static final String COLUMN_OBS = "observation";

    private static final String DATABASE_NAME = "donnee";
    private static final int DATABASE_VERSION = 1;

    // Commande sql pour la création de la base de données
    private static final String DATABASE_CREATE = "create table "
            + TABLE_COMMENTS + "(" + COLUMN_ID
            + " INTEGER primary key autoincrement, " + COLUMN_DATE
            + " TEXT not null, " + COLUMN_SYST
            + " FLOAT not null, " + COLUMN_DIAST
            + " FLOAT not null, " + COLUMN_POULS
            + " FLOAT not null, " + COLUMN_POIDS
            + " FLOAT, " + COLUMN_OBS
            + " TEXT)";

    public GestionBD(Context context){

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        System.out.println("Base de donnee creee");
        System.out.println(DATABASE_CREATE);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DATABASE_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        Log.w(GestionBD.class.getName(),
                "Upgrading database from version " + i + " to "
                        + i1 + ", which will destroy all old data");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_COMMENTS);
        onCreate(sqLiteDatabase);
    }
}
