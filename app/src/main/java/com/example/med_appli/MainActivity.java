package com.example.med_appli;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

import com.example.med_appli.adapter.MainAdapter;
import com.example.med_appli.adapter.basedonnee.GestionBD;
import com.example.med_appli.beans.Donnee;
import com.example.med_appli.controller.RequeteDonnee;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements DialogPage.ExampleDialogListener {
    TabLayout tabLayout;
    ViewPager viewPager;
    MainAdapter adapter;
    SharedPreferences pref;

    private TextView textViewUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pref = getSharedPreferences("document", 0);
        String presenceBD =pref.getString("bd", "0");
        System.out.println("bonjour "+pref.getString("bd", "0"));
        System.out.println("bonjour "+pref.getString("nom", "0"));

        Calendar calendar = Calendar.getInstance();
        final Date date = calendar.getTime();
        int day = date.getDay();
        int j = 0;
        if(presenceBD.equals("0")){
            GestionBD gestionBD = new GestionBD(getApplicationContext());
            openDialog();
            SharedPreferences.Editor edit = pref.edit();
            edit.putString("bd", "1");
            edit.commit();
            edit.apply();
            new GestionBD(getApplicationContext());
            System.out.println("bonjour "+pref.getString("bd", "0"));
        }

        tabLayout = findViewById(R.id.tablayout);
        viewPager = findViewById(R.id.viewpager);

        ArrayList<String> arrayList = new ArrayList<>();

        arrayList.add("Donnees generales");
        arrayList.add("Enregist jrnlier");


        prepareViewPager(viewPager, arrayList);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void prepareViewPager(ViewPager viewPager, ArrayList<String> arrayList) {
        adapter = new MainAdapter(getSupportFragmentManager());
        DonneeFragment donnee = new DonneeFragment();
        recordFragment recordFrag = new recordFragment();
        adapter.addFragment(donnee, arrayList.get(0));
        adapter.addFragment(recordFrag, arrayList.get(1));

    }

    public void openDialog() {
        DialogPage exampleDialog = new DialogPage();
        exampleDialog.show(getSupportFragmentManager(), "example dialog");
    }

    @Override
    public void applyTexts(String username) {
        SharedPreferences.Editor edit = pref.edit();
        edit.putString("nom", username);
    }
}