package com.example.med_appli.controller;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.med_appli.adapter.basedonnee.GestionBD;
import com.example.med_appli.beans.Donnee;
import com.github.mikephil.charting.data.Entry;

import java.util.ArrayList;
import java.util.List;

public class RequeteDonnee {

    public void ajout_donnee(Donnee m, GestionBD access){
        SQLiteDatabase db = access.getWritableDatabase();
        ContentValues content = new ContentValues();
        content.put("systolique", m.getSysto());
        content.put("diastolique", m.getDiasto());
        content.put("pouls", m.getPoul());
        content.put("date", m.getDate());
        content.put("poids", m.getPoids());
        content.put("observation", m.getComment());
        db.insert("donnee", null, content);
        db.close();
    }

    public ArrayList<Donnee> getAllDonnee(GestionBD access){
        ArrayList<Donnee> listDonnee = new ArrayList<>();
        SQLiteDatabase db = access.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM donnee ORDER BY date", null );
        if (cursor != null && cursor.getCount() != 0){
            cursor.moveToFirst();
            do {
                Donnee donnee = new Donnee(cursor.getString(1)
                        , cursor.getFloat(2)
                        , cursor.getFloat(3)
                        , cursor.getFloat(4)
                        , cursor.getFloat(5)
                        , cursor.getString(6));
                listDonnee.add(donnee);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return listDonnee;
    }

    public List<Donnee> getCont(GestionBD access, String debut, String fin){
        List<Donnee> listDonnee = new ArrayList<>();
        SQLiteDatabase db = access.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM donnee WHERE date BETWEEN '"+debut+"' AND '"+fin+"' ORDER BY date", null );
        if (cursor.moveToFirst()){
            do {
                Donnee don = new Donnee(cursor.getString(1), cursor.getFloat(2), cursor.getFloat(3), cursor.getFloat(4), cursor.getFloat(5), cursor.getString(6));
                listDonnee.add(don);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return listDonnee;
    }

    public List<Donnee> getDonnee(GestionBD access, String date){
        List<Donnee> listMessage = new ArrayList<>();
        SQLiteDatabase db = access.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM donnee WHERE date = ?", new String[]{date} );
        if (cursor.moveToFirst()){
            do {
                Donnee don = new Donnee(cursor.getString(1), cursor.getFloat(2), cursor.getFloat(3), cursor.getFloat(4), cursor.getFloat(5), cursor.getString(6));
                listMessage.add(don);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return listMessage;
    }

    public void delete_contenu(GestionBD access){
        SQLiteDatabase db = access.getWritableDatabase();
        db.close();

    }

    public int modifDonnee(Donnee donnee, GestionBD access){
        int result = 0;
        SQLiteDatabase db = access.getWritableDatabase();
        ContentValues content = new ContentValues();
        content.put("systolique", donnee.getSysto());
        content.put("diastolique", donnee.getDiasto());
        content.put("pouls", donnee.getPoul());
        content.put("date", donnee.getDate());
        content.put("poids", donnee.getPoids());
        content.put("observation", donnee.getComment());
        result = db.update("donnee", content, "date = ?", new String[]{donnee.getDate()});
        db.close();
        return result;
    }

    public int verifPoids(String date1, String date2, GestionBD access){
        List<Donnee> listMessage = new ArrayList<>();
        SQLiteDatabase db = access.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM donnee WHERE poids IS NOT NULL AND date BETWEEN '"+date1+"' AND '"+date2+"' ", null);
        if (cursor.moveToFirst()){
            do {
                Donnee don = new Donnee(cursor.getString(1), cursor.getFloat(2), cursor.getFloat(3), cursor.getFloat(4), cursor.getFloat(5), cursor.getString(6));
                listMessage.add(don);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        if (listMessage.size() == 0)
            return 0;
        else
            return 1;
    }

    public ArrayList<Donnee> getListDonnee(String date1, String date2, GestionBD access){
        ArrayList<Donnee> listMessage = new ArrayList<>();
        SQLiteDatabase db = access.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM donnee WHERE date BETWEEN '"+date1+"' AND '"+date2+"' ", null);
        System.out.println("SELECT * FROM donnee WHERE date BETWEEN '"+date1+"' AND '"+date2+"'");
        if (cursor.moveToFirst()){
            do {
                Donnee don = new Donnee(cursor.getString(1), cursor.getFloat(2), cursor.getFloat(3), cursor.getFloat(4), cursor.getFloat(5), cursor.getString(6));
                listMessage.add(don);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return listMessage;
    }

    public boolean verifDate(String date, GestionBD access){
        List<Donnee> listMessage = new ArrayList<>();
        SQLiteDatabase db = access.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM donnee WHERE date = ?", new String[]{date} );
        if (cursor.moveToFirst()){
            do {
                Donnee don = new Donnee(cursor.getString(1), cursor.getFloat(2), cursor.getFloat(3), cursor.getFloat(4), cursor.getFloat(5), cursor.getString(6));
                listMessage.add(don);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        if(listMessage.size() == 0){
            return false;
        }else{
            return true;
        }
    }

    public List<Entry> getListDonnee1(String date1, String date2, GestionBD access){
        List<Entry> listMessage = new ArrayList<>();
        SQLiteDatabase db = access.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM donnee WHERE date BETWEEN '"+date1+"' AND '"+date2+"' ", null);
        System.out.println("SELECT * FROM donnee WHERE date BETWEEN '"+date1+"' AND '"+date2+"'");
        if (cursor.moveToFirst()){
            do {
                //Donnee don = new Donnee(cursor.getString(1), cursor.getFloat(2), cursor.getFloat(3), cursor.getFloat(4), cursor.getFloat(5), cursor.getString(6));
                int d = Integer.parseInt(cursor.getString(1).split("/")[2]);
                Entry entry = new Entry(d, cursor.getFloat(3));
                listMessage.add(entry);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return listMessage;
    }
}
