package com.example.med_appli;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;

import com.github.barteksc.pdfviewer.PDFView;

import java.io.File;

public class OpenPDF extends AppCompatActivity {

    PDFView pdfView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_p_d_f);

        pdfView = (PDFView) findViewById(R.id.pdf_layout);

        Intent intent = this.getIntent();
        String chemin = intent.getExtras().getString("chemin");

        File file = new File(Environment.getExternalStorageDirectory(), chemin);
        boolean t = file.exists();
        System.out.println("file.exists() = " + file.exists());

        pdfView.fromFile(file).load();
    }
}